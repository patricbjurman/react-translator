// const url_add_users_local = "http://localhost:3004/users"
// const url_users = "http://localhost:3004/users?Name="
const url_heroku_fake_server = "https://fakeserverapp.herokuapp.com/users"
const url_heroku_fake_server_user = "https://fakeserverapp.herokuapp.com/users?Name="

/*
api.js: is responsible for all the api calls in this application.
method type: GET.
getUserByName gets a user from the database based on the input name.
return: a user object, data=userObject.
*/
export async function getUserByName(name) {
    try {
        let response = await fetch(url_heroku_fake_server_user + name);
        let data = await response.json();

        return data;
    }
    catch (error) {
        console.log(error);
    }
}

/*
method type: POST.
addNewUser adds a new user to the database. 
taking in name as an inparameter. 
*/
export async function addNewUser(name) {
    try {
        let user = {
            Name: name,
            posts: []
        }
        await fetch(url_heroku_fake_server, {
            method: 'POST',
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user) // body data type must match "Content-Type" header
        });
    } catch (error) {
        console.log(error);
    }

}

/*
method type: PUT
updateUserPosts adds a post to the posts list for a given user.

*/
export async function updateUserPosts(name, post) {
    let user = await getUserByName(name);
    let updatePosts = user[0].posts;
    let viewablePost = { post: post, view: true };
    updatePosts.push(viewablePost);
    let updatedUser = {
        Name: name,
        posts: updatePosts
    };

    try {
        await fetch(url_heroku_fake_server + "/" + user[0].id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(updatedUser)
        });
    }
    catch (error) {
        console.log(error);
    }
}

/*
method type: PUT
deletePosts is setting all the posts object to false.
when false, they will not be displayed.
*/
export async function deletePosts(name) {
    let user = await getUserByName(name);
    let deletePosts = user[0].posts;
    deletePosts.forEach(post => {
        post.view=false;
    });
    let userObj = {
        Name: name,
        posts: deletePosts,
    }

    try {
        await fetch(url_heroku_fake_server + "/" + user[0].id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userObj)
        });

    } catch (error) {
        console.log(error);
    }
    return deletePosts;
}

