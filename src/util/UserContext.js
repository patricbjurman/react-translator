import React, { createContext, useState } from 'react';


/*
UserContext: Utility.
Functions: Stores a string in a global context.
*/

export const UserContext = createContext(null);

export const UserProvider = ({ children }) => {
    const [user, setUser] = useState("");

    return (
        <UserContext.Provider value={{ user, setUser }}>
            {children}
        </UserContext.Provider>
    )
};