import "./Translation.css";
import { Button} from 'react-bootstrap'

/*
Child component: Handles the onClick for the button to translate a word to sign language. 
*/

function TranslationButton(props) {

    function buttonPressed()  {
        props.handleButtonPressed();
    }

    return (
        <div>
            <Button className="btnColor font" onClick={buttonPressed}>Translate</Button>{' '}
        </div>
    )
}

export default TranslationButton;