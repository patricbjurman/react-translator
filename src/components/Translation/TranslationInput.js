import { FormControl } from 'react-bootstrap'

/*
Child component: Handles the input from the user, notify parent when writing.   
*/

function TranslationInput(props) {

    function updateInput(e) {
        props.handleInput(e.target.value)
    }
    return (
        <div>
            <FormControl size="lg" aria-describedby="inputGroup-sizing-sm" onChange={(e) => updateInput(e)} />
        </div>
    )
}

export default TranslationInput;