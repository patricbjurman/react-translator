import "./TranslationStyle.css"

/*
Child component: Displays the images responding the sign language. 
*/

function TranslationImage(props) {
    return (
        <div className="Test">
            <img src={`./img/${props.img}.png`} alt="alt"></img>
        </div>
    )
}

export default TranslationImage;