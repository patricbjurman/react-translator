import { useState, useEffect, useContext } from "react"
import { notLoggedIn } from "../../hoc/withAuth"
import TranslationButton from "./TranslationButton";
import TranslationInput from "./TranslationInput";
import TranslationImage from "./TranslationImage";
import { updateUserPosts } from "../../util/api.js";
import { UserContext } from "../../util/UserContext";
import "./Translation.css";

/*
Translation page: Is a parent component.
Takes an input text and displays corresponding images in sign language.
Has three child components: TranslationInput, TranslationButton and TranslationImage. 
Function: Button pressed: takes a valid input, and add it to local storage, input is userName.
The page renders when the button is pressed and displays the new Images.
*/

function Translation() {

    const [input, setInput] = useState("");
    const [inputArray, setInputArray] = useState([]);
    const { setUser } = useContext(UserContext);

    const inputValue = <TranslationInput handleInput={inputHandler} />
    const inputButton = <TranslationButton handleButtonPressed={buttonPressed} />

    function inputHandler(e) {
        setInput(e);
    }

    function buttonPressed() {
        if (/^[a-zA-Z ]+$/.test(input)) {
            updateUserPosts(localStorage.getItem(["key"]), input);
            let inputArr = input.toLowerCase().replaceAll(" ", "ö").split("");
            console.log(inputArr);
            setInputArray(inputArr);
        } else {
            alert("Only english letters please.");
        }
    }

    useEffect(() => {
        setUser(localStorage.getItem(["key"]));
    }, [])

    const imgListRenderer = inputArray.map((object, i) => <TranslationImage key={i} img={object} alt="alt" />);

    return (
        <div>
            <div className="Translation">
                <div className="TranslationInput">
                    {inputValue}
                </div>
                <div className="TranslationBtn">
                    {inputButton}
                </div>
            </div>
            <div>
                {imgListRenderer}
            </div>
        </div>
    )
}

export default notLoggedIn(Translation)