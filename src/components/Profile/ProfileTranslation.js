import ProfileImage from "./ProfileImage.js"
import "./Profile.css";

/*
ProfileTranslation: Child component in Profile.
Shows the users translations.
*/

function ProfileTranslation(props) {
    const imgListRenderer = props.text ? props.text.toLowerCase().replaceAll(" ", "ö").split("").map((object, i) => <ProfileImage key={i} img={object} alt="alt" />) : "";
    return (
        <div className="profileTranslation">
            <div className="childOne">
                <h3 visible={props.visible}>{props.text}</h3>
            </div>
            <div className="childTwo">
                {imgListRenderer}
            </div>
        </div>
    )
}

export default ProfileTranslation;