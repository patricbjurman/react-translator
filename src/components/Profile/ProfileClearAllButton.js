import "./Profile.css";
import { Button} from 'react-bootstrap'

/*
Child component to Profile: notify the parent when clicked. 
*/

function ProfileClearAllButton(props) {

    function buttonPressed()  {
        props.handleButtonPressed();
    }

    return (
        <div>
            <Button className="btnColor font" onClick={buttonPressed}>Clear</Button>{' '}
        </div>
    )
}

export default ProfileClearAllButton;