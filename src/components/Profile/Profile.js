import { useState, useEffect, useContext } from "react"
import { useHistory } from "react-router-dom"
import ProfileClearAllButton from "./ProfileClearAllButton.js"
import ProfileLogOutButton from "./ProfileLogOutButton.js"
import ProfileTranslation from "./ProfileTranslation.js"
import { getUserByName, deletePosts } from "../../util/api.js";
import { notLoggedIn } from "../../hoc/withAuth"
import { UserContext } from "../../util/UserContext";
import "./Profile.css";

/*
Profile: Parent component.
Has four child components: ProfileClearAllButton, ProfileImage, ProfileLogOutButton and ProfileTranslation.
Functions: Shows the users last ten translations fetched from the database.
Have two buttons. One to clear the users search history and one to log the user out.
*/

function Profile() {

    const [translationArray, setTranslationArray] = useState([]);
    const clearButton = <ProfileClearAllButton handleButtonPressed={clearAllButton} />
    const { setUser } = useContext(UserContext);
    const history = useHistory();

    async function clearAllButton() {
        let updatePosts = await deletePosts(localStorage.getItem(["key"]))
        setTranslationArray([updatePosts]);
    }

    const logOutButton = <ProfileLogOutButton handleLogOutButton={logOutButtonFunc} />

    function logOutButtonFunc() {
        localStorage.clear();
        setUser("");
        history.push("/");
    }

    useEffect(() => {
        async function loadTranslations() {
            let arr = await getUserByName(localStorage.getItem("key"));
            setTranslationArray(arr[0].posts.slice(-10).filter(post => post.view));
        }
        setUser(localStorage.getItem("key"));
        loadTranslations();
    }, [])

    const translationList = translationArray.map((object, i) => <ProfileTranslation key={i} text={object.post} visible={object.visible} />);

    return (
        <div>
            <div>
                {translationList}
            </div>
            <div className="profileBtns">
                <div className="childBtn">
                    {clearButton}
                </div>
                <div className="childBtn2">
                    {logOutButton}
                </div>
            </div>
        </div >
    )
}

export default notLoggedIn(Profile);