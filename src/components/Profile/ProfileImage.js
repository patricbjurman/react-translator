import "./Profile.css";

/*
Child component to profile. 
Display an image. 
*/

function ProfileImage(props) {
    return (
        <div className="Test">
            <img className="profileImg" src={`./img/${props.img}.png`} alt="alt"></img>
        </div>
    )
}

export default ProfileImage;