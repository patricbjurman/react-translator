import "./Profile.css";
import { Button} from 'react-bootstrap'

/*
ProfileLogOutButton: Child component in Profile.
Handles the onClick to log the user out.
*/

function ProfileLogOutButton(props) {

    function buttonPressed()  {
        props.handleLogOutButton();
    }

    return (
        <div>
            <Button className="btnColor font" onClick={buttonPressed}>Log Out</Button>{' '}
        </div>
    )
}

export default ProfileLogOutButton;