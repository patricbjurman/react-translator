import { Navbar, Container } from 'react-bootstrap'
import { UserContext } from '../util/UserContext';
import { useContext } from "react"
import { useHistory } from "react-router-dom"
import "./NavbarComponent.css";

/*
Navbar component: Displays content about user, routes to profile page and translation page.
*/

function NavbarComponent() {

    const { user, setUser } = useContext(UserContext);
    const history = useHistory();

    function Click() {
        history.push("/profile");
    }

    function LogoClick() {
        history.push("/translation");
        setUser(localStorage.getItem("key"));
    }

    return (
        <div className="Nav">
            <Navbar>
                <Container>
                    <Navbar.Brand onClick={LogoClick} className="font navItem">LOST IN TRANSLATION</Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text className="font">
                            Signed in as: <a className="navItem" onClick={Click}>{user}</a>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default NavbarComponent;
