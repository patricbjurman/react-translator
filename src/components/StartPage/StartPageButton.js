import { Button} from 'react-bootstrap'
import "./StartPage.css";

/*
StartPageButton: Child component in StartPage.
Handles the onClick to log the user into the application.
*/

function StartPageButton(props) {

    function buttonPressed()  {
        props.handleButtonPressed();
    }

    return (
        <div>
            <Button className="btnColor font" onClick={buttonPressed}>Log in</Button>{' '}
        </div>
    )
}

export default StartPageButton