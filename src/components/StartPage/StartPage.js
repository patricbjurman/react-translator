import { useState, useContext } from "react"
import { useHistory } from "react-router-dom"
import StartPageInput from "./StartPageInput";
import StartPageButton from "./StartPageButton";
import { getUserByName, addNewUser } from "../../util/api.js";
import { alreadyLoggedIn } from "../../hoc/withAuth"
import { UserContext } from '../../util/UserContext';
import "./StartPage.css";


/*
StartPage: Parent component.
Has two child components: StartPageInput and StartPageButton.
Functions: Takes a valid input in the input field. Routes the application to translation page when button is pressed.
*/

function StartPage() {

    const [input, setInput] = useState("")
    const inputValue = <StartPageInput handleInput={inputHandler} />
    const inputButton = <StartPageButton handleButtonPressed={buttonPressed} />
    const history = useHistory();
    const { setUser } = useContext(UserContext);

    function inputHandler(input) {
        setInput(input)
    }

    async function buttonPressed() {
        if (input.length > 0) {
            let checkUser = await getUserByName(input);
            if (checkUser.length === 0) {
                console.log("in add new user");
                await addNewUser(input);
            }
            localStorage.setItem("key", input)
            setUser(input);
            history.push("/translation");
        } else {
            alert("Name can't be empty.");
        }

    }

    return (
        <div>
            <div className="Start">
                <div className="StartInput">
                    {inputValue}
                </div>
                <div className="StartBtn">
                    {inputButton}
                </div>
            </div>
        </div>
    )
}

export default alreadyLoggedIn(StartPage)