import { FormControl } from 'react-bootstrap'

/*
StartPageInput: Child component in StartPage.
Input field.
*/

function StartPageInput(props) {

    function updateInput(e) {
        props.handleInput(e.target.value);
    }

    return (
        <div>
            <FormControl size="lg" aria-describedby="inputGroup-sizing-sm" onChange={(e) => updateInput(e)} />
        </div>
    )
}

export default StartPageInput