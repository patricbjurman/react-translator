import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import StartPage from './components/StartPage/StartPage'
import Translation from './components/Translation/Translation'
import Profile from './components/Profile/Profile'
import { UserProvider } from './util/UserContext';
import NavbarComponent from './components/NavbarComponent';

function App() {

  return (
    <div className="App">
      <UserProvider>
        <BrowserRouter>
          <NavbarComponent />
          <Switch>
            <Route exact path="/" component={StartPage} />
            <Route exact path="/translation" component={Translation} />
            <Route exact path="/profile" component={Profile} />
          </Switch>
        </BrowserRouter>
      </UserProvider>
    </div>
  );
}

export default App;
