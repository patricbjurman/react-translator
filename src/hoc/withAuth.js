import { Redirect } from "react-router-dom"

/*
withAuth: High Order Function.
Functions: Re-routes the user if conditions isn't met. Otherwise renders <Component />
*/

export function notLoggedIn(Component) {
    return function() {
        if (localStorage.getItem("key") === null) {
            return <Redirect to="/" />
        }
        else {
            return <Component />
        }
    }
}

export function alreadyLoggedIn(Component){
    return function() {

        if(localStorage.getItem("key") !== null) {
            return <Redirect to="translation"/>
        }
        else {
            return <Component />
        }
    }
}